#!/usr/bin/env bash
set -ue

# --------------------------------------------------------------------
# Synopsis: List public scripts below _29r tree (scripts overview).  
#   Uses filter to ignore private, or "in work", or *._m4, and others.
# --------------------------------------------------------------------
# Usage: $ourname
# --------------------------------------------------------------------
# Options: none
# --------------------------------------------------------------------
# Rating: used: yes interesting: n stable: y TBDs: n provincial: n tone: _29r-meta
# --------------------------------------------------------------------

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2012/02/04 14:19:13 $   (GMT)
# $Revision: 1.29 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/main-2010.11.26/lib/sh/RCS/_29r_sh.m4.sed-src,v $
#      $Log: _29r_sh.m4.sed-src,v $
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2013 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

# *****************************************************************
# define env var dir-defs for our "uqjau tools"
# *****************************************************************
cd ${0%/*}/../../
# $(dirname $0)
  # cd 2 dirs up from ( ${0%/*} == $(dirname $0) )
  # if  $0 has no "/"; 'source' below fails/we abort, that's OK (we do not support . in path)
_29r=$PWD
cd "$OLDPWD"

cd ${0%/*}
   # dirname of $0
_29rev=${PWD##*/}
   # Basename of dir below 'commands', 'scommands' or 'lib'; it contains the soft link
   # to this script.
cd "$OLDPWD"

if ! test -f $_29r/package/$_29rev/main/etc/dirdefs; then
  # create default dirdefs 
  source $_29r/package/$_29rev/main/slib/seed_dirdefs.shinc
else
  source $_29r/package/$_29rev/main/etc/dirdefs
    # not security risk if $0 is in correct commands dir
    # dirdefs appends $_29rev suffix to some defs
fi

#--


source $_29lib/bash_common.shinc
  # for summary of what is provided run this:
  #   sed -n '2,/-end-of-help/p' $_lib/bash_common.shinc|less

case ${1:-} in
  --help)
    shift
    help "$@"
    exit
  ;;
esac

# ==================================================
# Process optional parameters.
# ==================================================

opt_true=1 OPTIND=1
  # OPTIND=1 only needed for 2nd and subsequent getopt invocations
  # since shell's start up sets it to 1

OPT_c=
while getopts :ac opt_char
do
   # Check for errors.
   # echo "opt_char:[$opt_char] OPTARG:[${OPTARG:-}] OPTIND:[$OPTIND]"
   #  $OPTIND is index of next arg to be processed (Ex: 1 == 1st arg)
   case $opt_char in
     \?) >&2 echo $ourname: unsupported option \(-$OPTARG\)
         # echo;help
         exit 3;
     ;;
     :) >&2 echo $ourname: missing argument for option \(-$OPTARG\)
         # Unfortunately this only works if the switch is last arg 
         # on command line.
         # echo;help
         exit 3;
     ;;
   esac

   case ${OPTARG:-} in
     -*)
       echo $ourname: [$OPTARG] not supported as arg to [-$opt_char]
       echo \ \ since it begins with -.
       # echo;help
       exit 3
     ;;
   esac

   # save info in an "OPT_*" env var.
   eval OPT_${opt_char}="\"\${OPTARG:-$opt_true}\""
done

shift $(( $OPTIND -1 ))
unset opt_true opt_char

_email_on_unexpected_exit=${_email_on_unexpected_exit:-yes}
_email_if_dying_on_trapped_sig=${_email_if_dying_on_trapped_sig:-no}
# _email_if_errlog_exists=yes # for Example "errlog foo" => e-mail

# =========================================================
# log creation and purging
# =========================================================
#log=$(mk_log_rotating -p 60 -d 1)
  # rotation: 60 periods of 1 day; log name prefix: {0..59}
#log=$(mk_log_expire_named -e 30)
  # logname contains a time to live string; -e 30 => expire after 30 days

# --------------------------------------------------------------------
#
#   read rcfile ( for env var defs );
#   local rcfile for local setting, else look for site setting
#
#   to see what dirs $_29eloc and $_29team are for:
#     awk '/## shared script rc files/, /^#-/' $_m/etc/dirdefs
#
# --------------------------------------------------------------------
for dir in $_29eloc $_29team
do
  rcfile=$dir/${ourname}rc
    # env vars impacted by rcfile:
    #   imp!! LIST_VARS_CHANGED_HERE
  if test -s "$rcfile"
  then
    source "$rcfile"
    break #prefer local file
  fi
done
unset rcfile dir
#--

source $_29lib/fileutils-misc.shinc

# ####################################################################
# Main procedure.
#   script start
# ####################################################################

# main
{

if [[ -n ${OPT_a:-} ]];then
  # disable ignores due to IGNORE and IGNORE2
  IGNORE='_NeVer_find_V0tu2n'
  IGNORE2=cat
else
  IGNORE='*.priv'
  IGNORE2="$(
  # using here doc for meta-quoting
  cat <<\__eohd
  egrep -v '/noshar|@(test|in-work)/|\._m4$|/(install|_pkg)$' 2>&1
__eohd
  )"
fi

scripts="$(

  (
  cd $_29r/package/$_29rev
  find $(_QUIET=1 _find_ig -L $(find . -maxdepth 1 -type l \( -name "$IGNORE" -prune -o -print \))  ) -not -type l  
  )   | eval $IGNORE2 |sort

)"

if [[ -n ${OPT_c:-} ]];then
  echo "$scripts"|awk -F/ '{print $2}'|uniq -c
else
  echo "$scripts"
fi

# ==================================================
# main done.
# ==================================================
: dropping through to main end #set -x makes this echo
exit 0

#main end
}

# Processing should never reach this line.
die "EOF scripting error, script should end w/an exit statement"

### End of File ###
