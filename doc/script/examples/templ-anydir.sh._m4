#!/usr/bin/env bash
set -ue

help() {

    sed -n "
        2,\$ {
            s/\${ourname}/${0##*/}/g;
            s/^    //; p;
        }" <<\____eohd

    NAME
        ${ourname} - short description here

    SYNOPSIS
        usage grammar here

    _COMMAND_SHORTNAME([<{ }>])

    DESCRIPTION                                 man diff, cp, du, false
        -q, --brief
            report only when files differ

        -s, --report-identical-files
            report when two files are the same

        -Z, --ignore-trailing-space
            ignore white space at line end

        -c, -C NUM, --context[=NUM]
            output NUM (default 3) lines of copied context

        -u, -U NUM, --unified[=NUM]
               output NUM (default 3) lines of unified context

        -e, --ed
            output an ed script

    OPTIONS                                                 man ed, tar
        -q, --brief
            report only when files differ

        -s, --report-identical-files
            report when two files are the same

        -Z, --ignore-trailing-space
            ignore white space at line end

    EXAMPLES

    FILES
        external tools invoked, config files

    ENVIRONMENT
        external env var dependencies

    LOGS

    EXIT CODES

    GLOBALS

    INPUTS

    OUTPUTS

    CALLED BY

    UPDATED: _m4_date
____eohd
}

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2012/02/04 14:19:13 $   (GMT)
# $Revision: 1.29 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/main-2010.11.26/lib/sh/RCS/_29r_sh.m4.sed-src,v $
#      $Log: _29r_sh.m4.sed-src,v $
#      Revision 1.29  2012/02/04 14:19:13  rodmant
#      *** empty log message ***
#
#      Revision 1.28  2011/05/22 11:40:52  rodmant
#      *** empty log message ***
#
#      Revision 1.27  2011/05/21 13:05:38  rodmant
#      *** empty log message ***
#
#      Revision 1.26  2011/05/21 12:18:04  rodmant
#      *** empty log message ***
#
#      Revision 1.17  2010/10/23 23:44:15  rodmant
#      *** empty log message ***
#
#      Revision 1.16  2010/10/23 12:46:34  rodmant
#      *** empty log message ***
#
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 20XX _Tom_Rodman_email
#
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# --------------------------------------------------------------------
# Define dir var definitions for _29r tree
#   for script located anywhere (ie not in _29r tree):
# --------------------------------------------------------------------

  eval "$(_29r_dirdefs)"

  # after all hosts have newer bash, try:
    # source <(_29r_dirdefs)
    # (fails in env -i ... in older bash)

#--

_29R_BASHCOMN

if [[ ${#} == 0 && a == a ]] ;then
    help
    exit
elif [[ ${1:-} == --help ]]; then
    shift
    help ""
    exit
fi

# ==================================================
# Process optional parameters.
# ==================================================

OPTIND=1
    # OPTIND=1 only needed for 2nd and subsequent getopt invocations
    # since shell's start up sets it to 1

#     leading : => 'getopts silent error reporting'
while getopts :fr:m: opt_char
do
    # Uncomment echo below to debug:
    # echo "opt_char:[$opt_char] OPTARG:[${OPTARG:-}] OPTIND:[$OPTIND]"
    case $opt_char in
        \?) 
            echo $ourname: unsupported option \(-$OPTARG\) >&2
            [[ -n ${FUNCNAME:-} ]] && return 1 || exit 3
        ;;
        :)  
            echo $ourname: missing argument for option \(-$OPTARG\) >&2
            # Only works if the switch is last arg  on command line.
            [[ -n ${FUNCNAME:-} ]] && return 1 || exit 3
        ;;
    esac

    case ${OPTARG:-} in
        -*)
            {
                echo $ourname: [$OPTARG] not supported as arg to [-$opt_char]
                echo since it begins with -.
            } >&2
            [[ -n ${FUNCNAME:-} ]] && return 1 || exit 3
        ;;
    esac

    # Save info in an "OPT_*" env var.
    eval OPT_${opt_char}="\"\${OPTARG:-TRUE}\""
done

# $OPTIND is index of next arg to be processed (1 == 1st arg)
shift $(( $OPTIND -1 ))
unset opt_char

_email_on_unexpected_exit=${_email_on_unexpected_exit:-yes}
_email_if_dying_on_trapped_sig=${_email_if_dying_on_trapped_sig:-no}
# _email_if_errlog_exists=yes # for Example "errlog foo" => e-mail

# =========================================================
# log creation and purging
# =========================================================
#log=$(mk_log_rotating -p 60 -d 1)
  # rotation: 60 periods of 1 day; log name prefix: {0..59}
#log=$(mk_log_expire_named -e 30)
  # logname contains a time to live string; -e 30 => expire after 30 days

# --------------------------------------------------------------------
#
#   read rcfile ( for env var defs );
#
#   to see what dirs $_29eloc and $_29team are for:
#     awk '/## shared script rc files/, /^#-/' $_m/etc/dirdefs
#
# --------------------------------------------------------------------
for dir in $_29team $_29eloc
do
  rcfile=$dir/${ourname}rc
    # env vars impacted by rcfile:
    #   imp!! LIST_VARS_CHANGED_HERE

  [[ -s $rcfile ]] && source "$rcfile"

done
unset rcfile dir
#--

# ####################################################################
# Main procedure.
#   script start
# ####################################################################

# main
{

# ==================================================
# main done.
# ==================================================
: dropping through to main end #set -x makes this echo
exit 0

#main end
}

# Processing should never reach this line.
die "EOF scripting error, script should end w/an exit statement"

### End of File ###
