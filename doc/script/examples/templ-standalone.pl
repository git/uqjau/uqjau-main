#!/usr/bin/env perl

use strict;
use warnings;
use Carp;
use English;

my $ourname;
($ourname  = $0) =~ s:.*/::g;

sub help()
{
  (my $out =  <<__EOHD ) =~ s/^  //msg ;
  --note: Using 'rsync --help' as an example.
  rsync is a file transfer program capable of efficient remote update
  via a fast differencing algorithm.
  
  Usage: rsync [OPTION]... SRC [SRC]... DEST
    or   rsync [OPTION]... SRC [SRC]... [USER@]HOST:DEST
    or   rsync [OPTION]... SRC [SRC]... [USER@]HOST::DEST
    or   rsync [OPTION]... SRC [SRC]... rsync://[USER@]HOST[:PORT]/DEST
    or   rsync [OPTION]... [USER@]HOST:SRC [DEST]
    or   rsync [OPTION]... [USER@]HOST::SRC [DEST]
    or   rsync [OPTION]... rsync://[USER@]HOST[:PORT]/SRC [DEST]
  The ':' usages connect via remote shell, while '::' & 'rsync://' usages
  connect to an rsync daemon, and require SRC or DEST to start with a module name.
  
  Options
   -v, --verbose               increase verbosity
   -q, --quiet                 suppress non-error messages
       --no-motd               suppress daemon-mode MOTD (see manpage caveat)
   -c, --checksum              skip based on checksum, not mod-time & size
   -a, --archive               archive mode; equals -rlptgoD (no -H,-A,-X)
       --no-OPTION             turn off an implied OPTION (e.g. --no-D)
   -r, --recursive             recurse into directories
   -R, --relative              use relative path names
       --no-implied-dirs       don't send implied dirs with --relative
   -b, --backup                make backups (see --suffix & --backup-dir)
       --backup-dir=DIR        make backups into hierarchy based in DIR
       --suffix=SUFFIX         set backup suffix (default ~ w/o --backup-dir)
  --snipped. Now, after options: 
  
  Here you put a more detailed description if required.
  It can be a paragraph or more.
__EOHD
  print "$out";
}

if ( $#ARGV + 1 == 0   or   $ARGV[0] eq "--help" )
  {
    help();
    exit 1;
  }

my $pathname = $ARGV[0];

if ( ! -e $pathname )
  {
    printf STDERR "$ourname: [$pathname] does not exist\n";
    exit 1;
  }

my $out = some_function ($pathname) ||
  croak "$ourname: ERROR:\n  [$!]\n";

print "$out\n";
exit 0
