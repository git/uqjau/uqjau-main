#!/usr/bin/env perl
use warnings;
use strict;

help() {

    sed -n "
        2,\$ {
            s/\${ourname}/${0##*/}/g;
            s/^    //; p;
        }" <<\____eohd

    NAME
        ${ourname} - short description here

    SYNOPSIS
        usage grammar here

    _COMMAND_SHORTNAME([<{ }>])

    DESCRIPTION                                 man diff, cp, du, false
        -q, --brief
            report only when files differ

        -s, --report-identical-files
            report when two files are the same

        -Z, --ignore-trailing-space
            ignore white space at line end

        -c, -C NUM, --context[=NUM]
            output NUM (default 3) lines of copied context

        -u, -U NUM, --unified[=NUM]
               output NUM (default 3) lines of unified context

        -e, --ed
            output an ed script

    OPTIONS                                                 man ed, tar
        -q, --brief
            report only when files differ

        -s, --report-identical-files
            report when two files are the same

        -Z, --ignore-trailing-space
            ignore white space at line end

    EXAMPLES

    FILES
        external tools invoked, config files

    ENVIRONMENT
        external env var dependencies

    LOGS

    EXIT CODES

    GLOBALS

    INPUTS

    OUTPUTS

    CALLED BY

    UPDATED: _m4_date
____eohd
}

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2012/02/04 14:19:13 $   (GMT)
# $Revision: 1.29 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/main-2010.11.26/lib/sh/RCS/_29r_sh.m4.sed-src,v $
#      $Log: _29r_sh.m4.sed-src,v $
#      Revision 1.29  2012/02/04 14:19:13  rodmant
#      *** empty log message ***
#
#      Revision 1.28  2011/05/22 11:40:52  rodmant
#      *** empty log message ***
#
#      Revision 1.27  2011/05/21 13:05:38  rodmant
#      *** empty log message ***
#
#      Revision 1.26  2011/05/21 12:18:04  rodmant
#      *** empty log message ***
#
#      Revision 1.17  2010/10/23 23:44:15  rodmant
#      *** empty log message ***
#
#      Revision 1.16  2010/10/23 12:46:34  rodmant
#      *** empty log message ***
#
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 20XX #<{_Tom_Rodman_email}>#
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

our($_29r, $_29rev, $ourname);

our($_29c, $_29lg, $_29sa, $_29db, $_29lib, $_29eloc, $_29esite, $_29team);
  # above should be consistent w/this hint:
  #  grep our $_29r/package/$_29rev/main/etc/dirdefs.perl

use Cwd 'realpath';
BEGIN{
# --------------------------------------------------------------------
# define directory definitions for our world, in several env vars
# --------------------------------------------------------------------
  my ($startdir, $dirname);
  $startdir= realpath;

  $0 =~ m{^(.*)/} and $dirname=$1;
  chdir "$dirname/../.." || die;
    #cd to parent dir of parent dir of parent dir of $0
    #we die if $0 has no "/"; that's OK

  $_29r= realpath;

  chdir "$startdir" || die;

  ($_29rev = $dirname ) =~ s~^.*/~~;
    # basename of $dirname

  require "$_29r/package/$_29rev/main/etc/dirdefs.perl";
    #not security risk if $0 is in correct commands dir

}

use Getopt::Std;

require("$_29lib/script_common.pl");

#getopts ex
#our($opt_x,$opt_d,$opt_p,$opt_o);
# getopts('xd:p:o:');
#   $opt_d would take on value of the arg to -d

