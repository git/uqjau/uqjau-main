#!/usr/bin/perl
# vim: set ft=perl

# ======================================================================
# This is your basic "Do I compile?" test.
# ======================================================================

use strict;
use File::Basename;

use Test::More tests => 1;
use Cwd q{abs_path};

BEGIN {
    my $ourdir = dirname($0);

    my $ourmod;
    ($ourmod = abs_path($0)) =~ s~/t/[^/]+$~~;
    $ourmod = basename($ourmod);

    chdir $ourdir
        || die "chdir $ourdir:$!";

    use lib "../../";

    use_ok($ourmod);
}
