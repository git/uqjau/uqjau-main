m4_divert(-1) #{
# ====================================================================
# Copyright (c) 2011, 2012 [_Tom_Rodman_email]
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

# m4 quotes are [] for this file

m4_define(XX_FANCY,[
# -------------------------------------------------------------------- 
# my_dir_is_smart_HOME_bin
# special function to support scripts in ~/bin
# consider replacing this function w/ 'false' if your script is
# not in ~/bin
# -------------------------------------------------------------------- 
my_dir_is_smart_HOME_bin()
{
  # 'my_dir' as in path to the dir containing *this script*

  # "smart_HOME_bin" as in $HOME/bin exists and has a
  # symbolic link named "_29lib".
  # User expected to put a symbolic link at ~/bin/_29lib pointing to $_29lib.
  # ( $_29lib is defined for ex. in .../package/cur/main/etc/dirdefs )

  local CWD_inode personal_bin_inode

  if ! test -L $HOME/bin/_29lib
  then
    return 1
  fi

  set $(command ls -id $_my_script_dir)
  CWD_inode=$1

  set $(ls -id $HOME/bin)
  personal_bin_inode=$1

  if test "$CWD_inode" = "$personal_bin_inode"
  then
    return 0
  else
    return 1
  fi
}

# -------------------------------------------------------------------- 
# deduce $_29r
#   -- $_29r is our root install dir  --
#      (usually a top level subdir of '/usr/local')
#   support scripts in ~/bin
# -------------------------------------------------------------------- 

_my_script_dir=${0%/*}
  # dirname of @S|@0

if my_dir_is_smart_HOME_bin
then
  # special case, script is in $HOME/bin
  cd -P $HOME/bin/_29lib
    # -P => follow link along link-independent path, see 'help cd'
  _29rev=$(pwd -P)
  _29rev=${_29rev##*/}
    # basename; _29rev could be say "cur" or "dev" or "beta"

  _29r=$(cd ../../;pwd -P)
    # -- $_29r is our root install dir  --
  cd "$OLDPWD"
  source $_29r/package/$_29rev/main/etc/dirdefs
    # not security risk if [$]0 is in correct commands dir
    # dirdefs appends $_29rev suffix to some defs
else
  # conventional case, ie not a script in ~/bin
XX_SIMPLE
fi
unset -f my_dir_is_smart_HOME_bin
unset _my_script_dir])

m4_define([XX_SCRIPT_ANYWHERE],
[# -------------------------------------------------------------------- 
# Define dir var definitions for _29r tree
#   for script located anywhere (ie not in _29r tree):
# -------------------------------------------------------------------- 

  eval "$(_29r_dirdefs)"

  # after all hosts have newer bash, try:
    # source <(_29r_dirdefs)
    # (fails in env -i ... in older bash)
])dnl

m4_define([XX_SIMPLE],[@<:@<{_29R_DIRDEF}>@:>@])
#}
m4_divert(0)dnl
dnl # ==================================================================== 
dnl # start of script
dnl # ==================================================================== 
#!/usr/bin/env bash
set -ue

_SH_MIN_SYNOPSIS

_SH_MIN_RCS_HEADER

# ====================================================================
# Copyright (c) 20XX @<:@<{_Tom_Rodman_email}>@:>@
#
# --------------------------------------------------------------------

# == Software License ==
# --

# ====================================================================
# test cases:
# ontology/tags:
# --------------------------------------------------------------------

dnl one of three macros should get expanded next:
m4_bmatch(XX_CHOICE,[fancy],[XX_FANCY],[simple],[XX_SIMPLE],[anywhere],[_29R_DIRDEF_SCRIPTANYWHERE])[]

#--

@<:@<{_29R_BASHCOMN}>@:>@

_SH_MIN_HELP_CLI_SW

_SH_MIN_GETOPTS

_29R_BASHCOMN_SET_EMAIL_FLAGS

_29R_BASHCOMN_LOGFILE_SETUP

_29R_READ_RC

# #################################################################### 
# Main procedure.
#   script start
# #################################################################### 

# main
{

# ==================================================
# main done.
# ==================================================
: dropping through to main end #set -x makes this echo
exit 0

#main end
}

# Processing should never reach this line.
die "EOF scripting error, script should end w/an exit statement"

### End of File ###
