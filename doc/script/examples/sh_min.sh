#!/usr/bin/env bash
set -ue

help() {

    sed -n "
        2,\$ {
            s/\${ourname}/${0##*/}/g;
            s/^    //; p;
        }" <<\____eohd

    NAME
        ${ourname} - short description here

    SYNOPSIS
        usage grammar here

    _COMMAND_SHORTNAME([<{ }>])

    DESCRIPTION                                 man diff, cp, du, false
        -q, --brief
            report only when files differ

        -s, --report-identical-files
            report when two files are the same

        -Z, --ignore-trailing-space
            ignore white space at line end

        -c, -C NUM, --context[=NUM]
            output NUM (default 3) lines of copied context

        -u, -U NUM, --unified[=NUM]
               output NUM (default 3) lines of unified context

        -e, --ed
            output an ed script

    OPTIONS                                                 man ed, tar
        -q, --brief
            report only when files differ

        -s, --report-identical-files
            report when two files are the same

        -Z, --ignore-trailing-space
            ignore white space at line end

    EXAMPLES

    FILES
        external tools invoked, config files

    ENVIRONMENT
        external env var dependencies

    LOGS

    EXIT CODES

    GLOBALS

    INPUTS

    OUTPUTS

    CALLED BY

    UPDATED: _m4_date
____eohd
}

# ====================================================================
# External tools used by $ourname                                                                   #  List scripts or tools ran or sourced by $ourname.  Define
#  variables below for each external tool, then invoke them using
#  the variable.
# ====================================================================
  # place external tool definitions here..   # usually delete this!

# ====================================================================
# Copyright (c) 20XX Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# -- Software License --
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---

ourname=${0##*/}
ourdir=${0%/$ourname}
  # may be useful dir for "rc" config files
Ourname=${0}

if [[ ${#} == 0 && a == a ]] ;then
    help
    exit
elif [[ ${1:-} == --help ]]; then
    shift
    help ""
    exit
fi

# ==================================================
# Process optional parameters.
# ==================================================

OPTIND=1
    # OPTIND=1 only needed for 2nd and subsequent getopt invocations
    # since shell's start up sets it to 1

#     leading : => 'getopts silent error reporting'
while getopts :fr:m: opt_char
do
    # Uncomment echo below to debug:
    # echo "opt_char:[$opt_char] OPTARG:[${OPTARG:-}] OPTIND:[$OPTIND]"
    case $opt_char in
        \?) 
            echo $ourname: unsupported option \(-$OPTARG\) >&2
            [[ -n ${FUNCNAME:-} ]] && return 1 || exit 3
        ;;
        :)  
            echo $ourname: missing argument for option \(-$OPTARG\) >&2
            # Only works if the switch is last arg  on command line.
            [[ -n ${FUNCNAME:-} ]] && return 1 || exit 3
        ;;
    esac

    case ${OPTARG:-} in
        -*)
            {
                echo $ourname: [$OPTARG] not supported as arg to [-$opt_char]
                echo since it begins with -.
            } >&2
            [[ -n ${FUNCNAME:-} ]] && return 1 || exit 3
        ;;
    esac

    # Save info in an "OPT_*" env var.
    eval OPT_${opt_char}="\"\${OPTARG:-TRUE}\""
done

# $OPTIND is index of next arg to be processed (1 == 1st arg)
shift $(( $OPTIND -1 ))
unset opt_char

# #################################################################### 
# Main procedure.
#   script start
# #################################################################### 

# main
{

# ==================================================
# main done.
# ==================================================
: dropping through to main end #set -x makes this echo
exit 0

#main end
}

# Processing should never reach this line.
echo "EOF scripting error, script should end w/an exit statement" >&2
exit 3

### End of File ###
