#!/bin/bash

set -eu
ourname=${0##*/}

help()
{
  sed -e 's~^    ~~'  >&2 <<____EOHD

    ====================================================================
    Synopsis: Delete all first level subdirs below
    /ahp_agents/ec/workspace, if their mtime is 31 days or older.
    ====================================================================
    Usage: $ourname [OPTION]...
    Options:
      -y              Required to enable the deletes.  Without this switch
                      the script shows what would have been deleted.

      -p              Prompt before each delete.

      -v              Verbose. Enables "set -x".

      -s              Sleep a few sec after considering a directory in
                      debug mode.

      -S              Skip initial sleep before real deletes.
    --------------------------------------------------------------------

____EOHD
}

if [[ ${1:-} == --help ]];then
  help
  exit 0
fi

