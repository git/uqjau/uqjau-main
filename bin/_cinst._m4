#!/usr/bin/env bash
set -eu

<<\____eohd

DESCRIPTION

    For installing one script into _29r tree.
    Assume CWD contains script, and is where script belongs.

SYNOPSIS

    _cinst [-d] SCRIPT-BASENAME
       # install to $_c
    _Cinst [-d] SCRIPT-BASENAME
       # install to $_C
    _linst [-d] SCRIPT-BASENAME
       # install to $_lib

OPTIONS

    -d
        Delete.

____eohd

trap '
  last_stat=$?
  last_command=${BASH_COMMAND-"OOPs BASH_COMMAND not defined"}
  echo last_command: $BASH_COMMAND, last stat: $last_stat
' ERR

# -------------------------------------------------------------------- 
# Copyright (c) 2010, 2018 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

ourname=${0##*/}

_29R_DIRDEF
#--

if [[ ${1:-} == -d ]];then

    action=delete
    shift
else
    action=install
fi

case $ourname in
    _cinst)
        dest=$_29c
    ;;
    _pcinst)
        dest=$_29pc
    ;;
    _Cinst)
        dest=$_29C
    ;;
    _linst)
        dest=$_29lib
    ;;
    _lpinst)
        dest=$_29libp
    ;;
esac

script_basename=${1}

[[ -e $script_basename ]] ||
{ 
    echo $ourname:WARNING: \[$PWD/$script_basename] refers to non existing file:
    command ls -ldog $PWD/$script_basename|sed -e 's~^~  ~'
    echo
} >&2

SRC_DIR=$PWD
    #   Assume CWD contains script, and is where script belongs.

package_par=$_29r/
SRC_DIR=${SRC_DIR/$package_par}
    #delete $package_par

cd $dest

case $action in
    install)
        ln -f -s ../../$SRC_DIR/$script_basename .

        mkdir -p RCS
        rm -f RCS/$script_basename
        cp -f -d $script_basename RCS

           # In $dest dir, $script_basename is a soft link. Backup the link in RCS subdir.
    ;;
    delete)
        rm -f $script_basename RCS/$script_basename
    ;;
esac
