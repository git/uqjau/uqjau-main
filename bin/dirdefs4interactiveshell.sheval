#!/usr/bin/env bash
: dirdefs4interactiveshell.sheval running .. impacts svars
:   args: $*
# output of this script should be eval'd by your interactive shell
#  Ex
#    eval "$($_t/bash/svars/dirdefs4interactiveshell.sheval)" ##

[[ ${_SETerrexit:-} = 1 ]] && set -e
  # Required even though a shell that may source us may already have 'set -e',
  # because the 'source' statement that calls us may followed by an '||'.
  #
  #  # ex: final '||' below disables errexit within source block
  #    $ (set -ex; source <( echo 'shopt -qo errexit || : OOPs;false' )  || : )
  #    ++ echo 'shopt -qo errexit || : OOPs;false'
  #    + source /dev/fd/63
  #    ++ shopt -qo errexit
  #    ++ : OOPs
  #    ++ false
  #    + :

# --------------------------------------------------------------------
# Synopsis: Define friendly (short) pathname env vars for accessing
# _29r dirs in interactive shell.  Called during bash login.
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2004, 2007, 2009, 2011 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2012/01/22 03:58:49 $   (GMT)
# $Revision: 1.21 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/main-2010.11.26/bin/RCS/dirdefs4interactiveshell.sheval._m4,v $
#      $Log: dirdefs4interactiveshell.sheval._m4,v $
#      Revision 1.21  2012/01/22 03:58:49  rodmant
#      *** empty log message ***
#
# --------------------------------------------------------------------

# --------------------------------------------------------------------
# hard coded defaults
# --------------------------------------------------------------------
  _r=${_r:-/usr/local/7Rq}
  _rev=cur

# --------------------------------------------------------------------
# parse CLI
# --------------------------------------------------------------------
case $# in
  1) _rev=$1 
  ;;
  2) _rev=$1 _r=$2
  ;;
  0): using defaults
  ;;
  *)
    echo '--'
    echo $0:ERROR: 0,1 or 2 CLI args pls >&2
    echo
  ;;
esac

builtin echo _rev=$_rev _r=$_r

builtin echo '
# none of these are intended to be used in scripts
# they are for humanly typed commandlines (hence our $0 name)

_c=$_29c
_C=$_29C
_lib=$_29lib
_pc=$_29pc
_libp=$_29libp

unset _29c _29C _29lib _29pc _29libp _29pkg
  # above vars for scripts only

_lg=$_T/log     # arbitrary/site specific
                # $_T gets a value through ...package/cur/main/etc/dirdefs

## consequence of "_29r tree" design:
_lj=$_lg/lj
_p=$_r/package/$_rev
_m=$_p/main
'

