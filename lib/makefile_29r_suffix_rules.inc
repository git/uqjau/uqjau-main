# TODO: This file is mis-named.  These are all the new *pattern* rules, since they contain %.  These are *not* suffix rules.

_m4ext=_m4
% : %.$(_m4ext)
	$(_29c)/_license_add $<
	_m4 $(_29lib)/_29r_sh.m4 $< > $@        # Notice '_m4' is not a filter of STDIN; neither is m4.
	test -f $@ && chmod +w $@
	chmod --reference=$< $@

_m4xext := _m4x
% : %.$(_m4xext)
	$(_29c)/_license_add $<
	_m42 < $< > $@                  # Both _M42 and _m42 process STDIN.
	test -f $@ && chmod +w $@
	chmod --reference=$< $@

anno_ext := anno
% : %.$(anno_ext)
	_rmcb < $< > $@
	@ # # 08:23:41 Sun 160612 0j 2 1243 35/256 w1 /var/home/rodmant
	@ # # psi rodmant $ _llt $_p|grep strip_my
	@ # /usr/local/7Rq/package/cur/t2h.priv-2011.05.26/shar/bin2/strip_my_t2h_comment_blocks00 <- /usr/local/7Rq/package/cur/t2h.priv-2011.05.26/shar/bin/_rmcb
	@ # /usr/local/7Rq/package/cur/t2h.priv-2011.05.26/shar/bin2/strip_my_t2h_comment_blocks00 <- /usr/local/7Rq/package/cur/t2h.priv-2011.05.26/shar/bin/_rm_comment_blocks00
	@ # /usr/local/7Rq/package/cur/t2h.priv-2011.05.26/shar/bin2/strip_my_t2h_comment_blocks00 <- /usr/local/7Rq/package/cur/t2h.priv-2011.05.26/shar/bin/_rm_comnt_blks0

### IMP: This suggests a general rule - stay away from this type of exanple!:
# This restricts us! Not sure if such a good idea?  Was used at Ringlead.  Commenting out on Thu 24 Aug 2017 
#_testext := _test
#%._m4 : %.$(_testext)
#	_rmcb < $< |\
#	perl -ne 'print "$$1\n" if(m{^_code:(.*$$)});' > $@

# Bad rule follows in that it says *txt depend on *pdf.  Can we kill it?
# commenting out on Thu 24 Aug 2017 
# %.txt : %.pdf
# 	pdftotext -layout $<  - | dos2unix  > $@

_M42ext := _M42
% : %.$(_M42ext)
	$(_29c)/_license_add $<
	_M42 < $< > $@                          # Both _M42 and _m42 process STDIN.
	test -f $@ && chmod +w $@
	chmod --reference=$< $@

_adocext := _adoc
%.html : %.$(_adocext)
	sed -e 's/#\..*//' < $< |asciidoc    -dbook - > $@

%._html : %.$(_adocext)
	sed -e 's/#\..*//' < $< |asciidoc -s -dbook - > $@

# Prevent re-make of this makefile.
makefile_29r_suffix_rules.inc : ;
RCS/makefile_29r_suffix_rules.inc : ;

