m4_divert(-1)
# --------------------------------------------------------------------
# Synopsis: shell script macros for _29r_street_name project.
# --------------------------------------------------------------------
# Usage: Your shell script should be named w/suffix "._m4".
#   From interactive shell run
#   make -rf $_lib/makefile_29r SCRIPTNAME-WITHOUT-SUFFIX
#   This has a make pattern rule, that relies on script $_C/_m4.
# --------------------------------------------------------------------

# IMPORTANT: all instances of $0 $1 $2 $3 ... should refer *only*to
# m4 definition arguments:
#   :g;$[0-9];#

# ----------------------------------------------------------------------------
# $Source: /usr/local/share/3reu/7Rq/package/cur/main-2010.11.26/lib/sh/RCS/_29r_sh.m4.sed-src,v $
# $Date: 2017/09/07 21:04:49 $    $Revision: 1.91 $
# ----------------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2010-2015 Tom Rodman <Rodman.T.S@gmail.com>
#
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>
# ====================================================================

m4_define(shell_comment_2_tex,[<{m4_patsubst($1,[<{^#}>],[<{%}>])}>])

# ex
#  shell_comment_2_tex([<{_29R_GROUP_LICENSE}>])

m4_define(_Tom_Rodman_email,[<{Tom Rodman <Rodman.T.S@gmail.com>}>])

m4_define(_COMMAND_SHORTNAME,[<{# Short name: $1 ( symbolic link in _29r commands dir )}>])

m4_define(_interact_or_script_funct,
[<{: _func_ok2unset_ Source is in a _29r ".shinc" lib file, so usable for interactive shell or script.}>])

m4_define(_corefunct_interact_or_not,
[<{m4_dnl
: _func_ok2unset_ Source is in a _29r ".shinc" lib file, so usable for interactive shell or script.
    : Exceptional, since NON interactive shells source this in ~/.bashrc.
}>])

m4_define(_nofork_interact_or_script_funct,
[<{: _func_ok2unset_ Is 'no-fork' function, so do not use in script that only-has-this-code.
    : Source is in a _29r ".shinc" lib file, usable for interactive shell or script, where script itself internally calls this function.}>])

m4_define(_STANDALONE_FILE_LICENSE,
[<{# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.}>]m4_dnl
)

m4_define(_29R_GROUP_LICENSE,
[<{[<{# This file is part of }>]_29r_street_name[<{.
#
# }>]_29r_street_name[<{ is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# }>]_29r_street_name[<{ is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with }>]_29r_street_name[<{.  If not, see <http://www.gnu.org/licenses/>.}>]}>]m4_dnl
)

m4_define(_29R_NO_HELP_NEEDED,
[<{user_needs_no_help() {

    # Give help, and return 1
    # if no args
    # or first arg is: '-h' or '--help'.

    if [[    @S|@# == 0
          || @S|@1 =~ ^(-h|--help)@S|@ ]];then

        ${_help_msg_function:-usage}
        return 1
    else
        # Happy case/no help needed.
        return 0
    fi

    # ex
    #     user_needs_no_help "@S|@@" || exit 1
}}>]m4_dnl
)

m4_define(_29R_NL_ESCAPE,
    [<{m4_patsubst([<{$1}>], [<{$}>], [<{\\}>])}>]m4_dnl
)

m4_define(_29R_NL_TO_GS_stable,
    [<{m4_patsubst([<{$1}>], [<{
}>], [<{}>])}>]m4_dnl
)

m4_define(_29R_NL_TO_GS,
    [<{m4_pushdef([<{_foo}>],[<{m4_chomp_all([<{$1}>])m4_popdef([<{_foo}>])}>])m4_patsubst(_foo, [<{
}>], [<{}>])}>]m4_dnl
)

m4_define(_29R_SCRIPT_HELP_MINIMAL,
[<{--note: Using 'rsync --help' as an example.
rsync is a file transfer program capable of efficient remote update
via a fast differencing algorithm.

Usage: rsync [OPTION]... SRC [SRC]... DEST
  or   rsync [OPTION]... SRC [SRC]... [USER@]HOST:DEST
  or   rsync [OPTION]... SRC [SRC]... [USER@]HOST::DEST
  or   rsync [OPTION]... SRC [SRC]... rsync://[USER@]HOST[:PORT]/DEST
  or   rsync [OPTION]... [USER@]HOST:SRC [DEST]
  or   rsync [OPTION]... [USER@]HOST::SRC [DEST]
  or   rsync [OPTION]... rsync://[USER@]HOST[:PORT]/SRC [DEST]
The ':' usages connect via remote shell, while '::' & 'rsync://' usages
connect to an rsync daemon, and require SRC or DEST to start with a module name.

Options
 -v, --verbose               increase verbosity
 -q, --quiet                 suppress non-error messages
     --no-motd               suppress daemon-mode MOTD (see manpage caveat)
 -c, --checksum              skip based on checksum, not mod-time & size
 -a, --archive               archive mode; equals -rlptgoD (no -H,-A,-X)
     --no-OPTION             turn off an implied OPTION (e.g. --no-D)
 -r, --recursive             recurse into directories
 -R, --relative              use relative path names
     --no-implied-dirs       don't send implied dirs with --relative
 -b, --backup                make backups (see --suffix & --backup-dir)
     --backup-dir=DIR        make backups into hierarchy based in DIR
     --suffix=SUFFIX         set backup suffix (default ~ w/o --backup-dir)
--snipped. Now, after options:

Here you put a more detailed description if required.
It can be a paragraph or more.}>]m4_dnl
)

m4_define(_29R_DIRDEF_SCRIPTANYWHERE,
[<{# --------------------------------------------------------------------
# Define dir var definitions for _29r tree
#   for script located anywhere (ie not in _29r tree):
# --------------------------------------------------------------------

  eval "$(_29r_dirdefs)"

  # after all hosts have newer bash, try:
    # source <(_29r_dirdefs)
    # (fails in env -i ... in older bash)}>]m4_dnl
)

m4_define(_29R_DIRDEF,
[<{# *****************************************************************
# define env var dir-defs for our "_29r_street_name tools"
# *****************************************************************

if [[ -n ${BASH_SOURCE[0]} ]];then
    # We are being sourced.
    cd ${BASH_SOURCE[0]%/*}/../../
else
    cd ${0%/*}/../../
        # cd 2 dirs up from ( ${0%/*} == $(dirname [<{$}>]0)
        # if [<{$}>]0 has no "/"; 'source' below fails/we abort, that's OK (we do not support . in path)
fi

_29r=$PWD
cd "$OLDPWD"

if [[ -n ${BASH_SOURCE[0]} ]];then
    cd ${BASH_SOURCE[0]%/*}
else
    cd ${0%/*}
        # $(dirname [<{$}>]0)
fi

_29rev=${PWD##*/}
   # Basename of dir below 'commands', 'scommands' or 'lib'; it contains the soft link
   # to this script.
cd "$OLDPWD"

if ! test -f $_29r/package/$_29rev/main/etc/dirdefs; then
  # create default dirdefs
  source $_29r/package/$_29rev/main/slib/seed_dirdefs.shinc
else
  source $_29r/package/$_29rev/main/etc/dirdefs
    # not security risk if [<{$}>]0 is in correct commands dir
    # dirdefs appends $_29rev suffix to some defs
fi}>]m4_dnl
)

m4_define(_29R_DIRDEF2,
[<{_PDIR_BASENAME_FUNCTIONS

# *****************************************************************
# define env var dir-defs for our "_29r_street_name tools"
# *****************************************************************

ourpathname=${BASH_SOURCE[0]:-${0}}

# For newer bash, ourpathname is ${BASH_SOURCE[0]}:
#   Goal: define dirdefs even if we are being sourced;
#   this requires ${BASH_SOURCE[0]} be supported by bash.
#   Then, AFAIK, ${0} == ${BASH_SOURCE[0]}, unless we're being sourced.

_29rev=$(_pdir_basename_abs "$ourpathname")

# define _29r {
cd ${ourpathname%/*}/../../
  # cd 2 dirs up from ( ${ourpathname%/*} == $(dirname ${ourpathname}) )
_29r=$PWD
cd "$OLDPWD"
#             }

unset ourpathname
unset -f _pdir_basename_abs _dotted_pathname _pdir_basename_by_cd _pdir_basename_by_pn_parse

if ! test -f $_29r/package/$_29rev/main/etc/dirdefs; then
  # create default dirdefs
  source $_29r/package/$_29rev/main/slib/seed_dirdefs.shinc
else
  source $_29r/package/$_29rev/main/etc/dirdefs
    # not security risk if [<{$}>]0 is in correct commands dir
    # dirdefs appends $_29rev suffix to some defs
fi}>]m4_dnl
)

m4_define(_29R_DIRDEF_PERL,
[<{m4_dnl
use Cwd 'realpath';
# --------------------------------------------------------------------
# define directory definitions for our world, in several _29* env vars
# --------------------------------------------------------------------
  my ($startdir, $dirname);
  $startdir= realpath;

  #TBD: Is this stanza clean?:
  ${0} =~ m{^(.*)/} and $dirname=${1};
  chdir "$dirname/../.." || die;
    # cd to parent dir of parent dir of parent dir of ${0}
    # we die if ${0} has no "/"; that's OK

  my $_29r = realpath;

  chdir "$startdir" || die;

  my $_29rev;
  ($_29rev = $dirname ) =~ s~^.*/~~;
    # basename of $dirname
  undef $dirname;

  # RE: method to source $_29r/package/$_29rev/main/etc/dirdefs.perl.v2
    # Alt approach using 'require FILENAME' seems to create a separate
    # lexical scope, which results in needing to use 'our' instead of
    # my.  So *not* using require.
    #
    # TBD, see if we can adopt 'use' instead of below slurp/eval.

  open(my $fh, "<", "$_29r/package/$_29rev/main/etc/dirdefs.perl.v2") || die "$!";
    # At least we should die here in invoked incorrectly (ie ./ourname for some cases).

  my @tmp = <$fh>;
  close $fh;undef $fh;
  my $tmp = join( "\n",@tmp);
  undef @tmp;

  # eval should define vars shown by 'my' below:
  my ($_29c,$_29lib,$_29eloc,$_29team,$_29lg);
  eval $tmp;
    # TBD: die if eval had errs
  undef $tmp;
}>]m4_dnl
)

m4_define(_PDIR_BASENAME_FUNCTIONS,
[<{# --------------------------------------------------------------------
# _pdir_basename_abs(): to return the basename of the parent dir for a pathname:
#   depends on:
#     _dotted_pathname , _pdir_basename_by_cd, _pdir_basename_by_pn_parse
#   used by: [<{_29R_DIRDEF2}>]
# --------------------------------------------------------------------

_dotted_pathname()
{
(
set -eu
pn=${1}

oIFS=$IFS
IFS=/
for i in $pn;do
  [[ ${i} = . || ${i} = .. ]] && dotted_path=1
done

[[ -n ${dotted_path:-} ]] && return 0 || return 1
)

}

_pdir_basename_by_cd()
{
  # parent dir of pathname must exist on filesystem
  (
  set -eu
  pn=${1%/}
    # remove slash at end
  pdir="${pn%/*}"
    # dirname  ( assume: last pathname component is not . or .., it is simple text word )
  cd "$pdir"
  echo "${PWD##*/}"
    # basename of dir
  )
}

_pdir_basename_by_pn_parse()
{
  # only works on pathnames where parent dir exists on filesystem

  # Advantage: can report symbolic link name when
  # pathname parent dir is defined through symbolic link.

  # Short coming: dotted pathnames
  (
  set -eu
  pn=${1%/}
    # remove slash at end if there
  out="${pn%/*}"
    # parent dir of $pn
  [[ -e "$out" || -L "$out" ]] ||
  {
    echo $FUNCNAME:ERROR for \[$pn],  \[$out] may not be real pathname >&2
    return 1
  }

  echo "${out##*/}"
    # basename of $out
  )
}

_pdir_basename_abs()
{
(
  set -eu
  pn=${1%/}
    # remove slash at end if there

  if _dotted_pathname "$pn";then
    _pdir_basename_by_cd       "$pn"
  else
    _pdir_basename_by_pn_parse "$pn"
  fi
)
}}>]m4_dnl
)

m4_define(_29R_DIRDEF_unless_sourced,
[<{
if [[ -z ${BASH_SOURCE[0]:-} || ${BASH_SOURCE[0]:-} = ${0} ]];then

# {
# We are probably inside a script; ie *not* being sourced.  May also be an older bash.
# Old versions of bash did not have BASH_SOURCE array.

_29R_DIRDEF
# }

else

# probably being sourced
:

fi
}>]m4_dnl
)

m4_define(_SH_MIN_HELP_SIMPLE_FN,
[<{help() {

    sed -n "
        2,\$ {
            s/\${ourname}/${0##*/}/g;
            s/^    //; p;
        }" <<\____eohd

    NAME
        ${ourname} - short description here

    SYNOPSIS
        usage grammar here

_SH_MIN_SYNOPSIS_DESCRIPTION

_SH_MIN_SYNOPSIS_OPTIONS

    EXAMPLES
        ${ourname} [your example here ...]

    FILES
        external file dependencies

    ENVIRONMENT
        external env var dependencies
____eohd

}}>]m4_dnl
)

m4_define(_SH_MIN_HOMERC,
[<{# --------------------------------------------------------------------
# source ~/.${ourname}rc if it exists
# --------------------------------------------------------------------
rcfile=$HOME/.${ourname}rc
  # env vars impacted by rcfile:
  #   imp!! LIST_VARS_CHANGED_HERE
if test -s "$rcfile"
then
  [[ -n $_verbose ]] && set -x
  source "$rcfile"
  [[ -n $_verbose ]] && set +x
fi
unset rcfile
}>]m4_dnl
)

m4_define(_SH_MIN_SYNOPSIS_OPTIONS,[<{m4_dnl
    OPTIONS                                                 man ed, tar
        -q, --brief
            report only when files differ

        -s, --report-identical-files
            report when two files are the same

        -Z, --ignore-trailing-space
            ignore white space at line end}>]m4_dnl
)

m4_define(_SH_MIN_SYNOPSIS_DESCRIPTION,[<{m4_dnl
    DESCRIPTION                                 man diff, cp, du, false
        -q, --brief
            report only when files differ

        -s, --report-identical-files
            report when two files are the same

        -Z, --ignore-trailing-space
            ignore white space at line end

        -c, -C NUM, --context[=NUM]
            output NUM (default 3) lines of copied context

        -u, -U NUM, --unified[=NUM]
               output NUM (default 3) lines of unified context

        -e, --ed
            output an ed script}>]m4_dnl
)

m4_define(_SH_MIN_SYNOPSIS_OLD,[<{m4_dnl
# ======================================================================
# SYNOPSIS: multiline synopsis
# would go here
# --------------------------------------------------------------------
# [<{_COMMAND_SHORTNAME(}>]@<:@<{ }>@:>@)
# --------------------------------------------------------------------
# USAGE:
# --------------------------------------------------------------------
# OPTIONS:
# --------------------------------------------------------------------
# EXAMPLES:
# --------------------------------------------------------------------
# FILES:
#   external tools invoked, config files
# --------------------------------------------------------------------
# ENVIRONMENT:
#   external env var dependencies
# --------------------------------------------------------------------
# DESCRIPTION:
# --------------------------------------------------------------------
# LOGS:
# --------------------------------------------------------------------
# EXIT CODES:
# --------------------------------------------------------------------
# GLOBALS:
# --------------------------------------------------------------------
# INPUTS:
# --------------------------------------------------------------------
# OUTPUTS:
# --------------------------------------------------------------------
# CALLED BY:
# --------------------------------------------------------------------}>]m4_dnl
)

m4_define(_SH_MIN_SYNOPSIS,[<{m4_dnl
m4_dnl Keep at top of script. Stop practice of synopsis being outside help function,
help() {

    sed -n "
        2,\$ {
            s/\${ourname}/${0##*/}/g;
            s/^    //; p;
        }" <<\____eohd

    NAME
        ${ourname} - short description here

    SYNOPSIS
        usage grammar here

    [<{_COMMAND_SHORTNAME(}>]@<:@<{ }>@:>@)

_SH_MIN_SYNOPSIS_DESCRIPTION

_SH_MIN_SYNOPSIS_OPTIONS

    EXAMPLES

    FILES
        external tools invoked, config files

    ENVIRONMENT
        external env var dependencies

    LOGS

    EXIT CODES

    GLOBALS

    INPUTS

    OUTPUTS

    CALLED BY

    UPDATED: [<{_m4_date}>]
____eohd
}}>]m4_dnl
)

m4_define(_SH_MIN_RCS_TINY_HEADER,
[<{# --------------------------------------------------------------------
# $Source: /usr/local/share/3reu/7Rq/package/cur/main-2010.11.26/lib/sh/RCS/_29r_sh.m4.sed-src,v $
# $Date: 2017/09/07 21:04:49 $ GMT    $Revision: 1.91 $
# --------------------------------------------------------------------}>]m4_dnl
)

m4_define(_SH_MIN_RCS_HEADER,
[<{# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2017/09/07 21:04:49 $   (GMT)
# $Revision: 1.91 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker:  $
#   $Source: /usr/local/share/3reu/7Rq/package/cur/main-2010.11.26/lib/sh/RCS/_29r_sh.m4.sed-src,v $
#      $Log: _29r_sh.m4.sed-src,v $
#      Revision 1.91  2017/09/07 21:04:49  rodmant
#      *** empty log message ***
#
#      Revision 1.29  2012/02/04 14:19:13  rodmant
#      *** empty log message ***
#
#      Revision 1.28  2011/05/22 11:40:52  rodmant
#      *** empty log message ***
#
#      Revision 1.27  2011/05/21 13:05:38  rodmant
#      *** empty log message ***
#
#      Revision 1.26  2011/05/21 12:18:04  rodmant
#      *** empty log message ***
#
#      Revision 1.17  2010/10/23 23:44:15  rodmant
#      *** empty log message ***
#
#      Revision 1.16  2010/10/23 12:46:34  rodmant
#      *** empty log message ***
#
# --------------------------------------------------------------------}>]m4_dnl
)

m4_define(_29R_READ_RC,
[<{# --------------------------------------------------------------------
#
#   read rcfile ( for env var defs );
#
#   to see what dirs $_29eloc and $_29team are for:
#     awk '/## shared script rc files/, /^#-/' $_m/etc/dirdefs
#
# --------------------------------------------------------------------
for dir in $_29team $_29eloc
do
  rcfile=$dir/${ourname}rc
    # env vars impacted by rcfile:
    #   imp!! LIST_VARS_CHANGED_HERE

  [[ -s $rcfile ]] && source "$rcfile"

done
unset rcfile dir
#--}>]m4_dnl
)

m4_define(_PERL_GETOPTSLONG,
[<{
use Getopt::Long qw( :config bundling no_ignore_case );

GetOptions('foo|v' => \my $opt_verbose)
    or die("usage\n");

print( $opt_verbose || 0, "\n");
# see http://www.perlmonks.org/?node_id=806698       ( Re: How can I have both short and long options with Getopt::Long? )
}>]
)

m4_define(_SH_LOFI_GETOPTS,
[<{# WARNING: minimal version..; normal getopts error reporting
# ==================================================
# parse commandline switches
# ==================================================
local opt_true=1 opt_char= badOpt=
OPTIND=1
  # OPTIND=1 for 2nd and subsequent getopt invocations; 1 at shell start

local OPT_f= OPT_r= OPT_m=
while getopts fr:m: opt_char                               # no leading : => normal getopts error reporting
do
    # save info in an "OPT_*" env var.
    [[ $opt_char != \? ]] && eval OPT_${opt_char}="\"\${OPTARG:-$opt_true}\"" ||
        badOpt=1
done
shift $(( $OPTIND -1 ))

# If badOpt:  If in function return 1, else exit 1:
[[ -z $badOpt ]] || { : help; return 1 &>/dev/null || exit 1; }

#unset opt_true opt_char badOpt}>]
)

m4_define([<{_MAILTO}>], [<{[$1](mailto:%22}>][<{m4_patsubst($1, [<{\ }>], [<{%20}>])}>][<{%22%20%3c}>][<{$2}>][<{%3e)}>])

m4_define(_SH__hostname_short,
[<{_hostname_short () {
    # --------------------------------------------------------------------
    # SYNOPSIS: echo hostname, without DNS domain
    # --------------------------------------------------------------------
    _interact_or_script_funct

    local out
    if out=$(uname -n); then
        # out=${HOSTNAME:-${COMPUTERNAME:-$(
        #     exec 2>/dev/null;hostname || uname -n || exit 1)}}; then

        echo ${out%%.*};
    else
        echo $FUNCNAME:ERROR: could not determine hostname >&2;
        return 1;
    fi
}}>]m4_dnl
)

m4_define(_SH_MIN_GETOPTS,
[<{# ==================================================
# Process optional parameters.
# ==================================================

OPTIND=1
    # OPTIND=1 only needed for 2nd and subsequent getopt invocations
    # since shell's start up sets it to 1

#     leading : => 'getopts silent error reporting'
while getopts :fr:m: opt_char
do
    # Uncomment echo below to debug:
    # echo "opt_char:[$opt_char] OPTARG:[${OPTARG:-}] OPTIND:[$OPTIND]"
    case $opt_char in
        \?)
            echo $ourname: unsupported option \(-$OPTARG\) >&2
            [[ -n ${FUNCNAME:-} ]] && return 1 || exit 3
        ;;
        :)
            echo $ourname: missing argument for option \(-$OPTARG\) >&2
            # Only works if the switch is last arg  on command line.
            [[ -n ${FUNCNAME:-} ]] && return 1 || exit 3
        ;;
    esac

    case ${OPTARG:-} in
        -*)
            {
                echo $ourname: [$OPTARG] not supported as arg to [-$opt_char]
                echo since it begins with -.
            } >&2
            [[ -n ${FUNCNAME:-} ]] && return 1 || exit 3
        ;;
    esac

    # Save info in an "OPT_*" env var.
    eval OPT_${opt_char}="\"\${OPTARG:-TRUE}\""
done

# $OPTIND is index of next arg to be processed (1 == 1st arg)
shift $(( $OPTIND -1 ))
unset opt_char}>]m4_dnl
)

m4_define(_SH_MIN_HELP_CLI_SW,
[<{if [[ ${#} == 0 && a == a ]] ;then
    help
    exit
elif [[ ${1:-} == --help ]]; then
    shift
    help "$@"
    exit
fi}>]m4_dnl
)

m4_define(_29R_BASHCOMN,
[<{source $_29lib/bash_common.shinc
  # for summary of what is provided run this:
  #   sed -n '2,/-end-of-help/p' $_lib/bash_common.shinc|less}>]m4_dnl
)

m4_define(_29R_BASHCOMN_SET_EMAIL_FLAGS,
[<{_email_on_unexpected_exit=${_email_on_unexpected_exit:-yes}
_email_if_dying_on_trapped_sig=${_email_if_dying_on_trapped_sig:-no}
# _email_if_errlog_exists=yes # for Example "errlog foo" => e-mail}>]m4_dnl
)

m4_define(_29R_BASHCOMN_LOGFILE_SETUP,
[<{# =========================================================
# log creation and purging
# =========================================================
#log=$(mk_log_rotating -p 60 -d 1)
  # rotation: 60 periods of 1 day; log name prefix: {0..59}
#log=$(mk_log_expire_named -e 30)
  # logname contains a time to live string; -e 30 => expire after 30 days}>]m4_dnl
)

m4_define(_TCL_MIN_OURVARS,
[<{set ourname $argv0
regsub .*/ $ourname "" ourname}>]m4_dnl
)

m4_dnl TODO: Review _TCL_GETOPT; specifically are $1 and $2 truly macro args:
m4_define(_TCL_GETOPT,
[<{set optstring $1

# use external GNU getopt, save results to $getopt_out_list

set job {set -eu; eval set - $(getopt "[<{$}>]@");for arg;do echo -n "{$arg} ";done}
set cmd [concat exec sh -c \{$job\} us -o $optstring -n $ourname -- $argv]
  # concat so $argv may be expanded into multiple elements
#puts [eval $cmd]; exit
  # works
    # ex
    #   $ THISSCRIPT -Pu foo
    #   {-P} {-u} {foo} {--}

if [catch [concat $cmd] getopt_out_list] {
  puts stderr $getopt_out_list
  exit 1
}

## parse $getopt_out_list

_TCL_SHIFT

while {[llength $getopt_out_list]} {
  switch -glob -- [lindex $getopt_out_list 0] {
    $2
    --* { shift {getopt_out_list} ; break }
  }
}
}>]m4_dnl
)

m4_define(_SH_MIN_OURVARS,
[<{ourname=${0##*/}
ourdir=${0%/$ourname}
  # may be useful dir for "rc" config files
Ourname=${0}}>]m4_dnl
)

m4_define(_TCL_SHIFT,
[<{proc shift {ls} {
  # Adly Abdullah http://wiki.tcl.tk/16032
  upvar 1 $ls LIST
    # allows arg passed to this procedure to get changed by this procedure
  if {[llength $LIST]} {
    set ret [lindex $LIST 0]
    set LIST [lreplace $LIST 0 0]
       # delete 1st element in $LIST
    return $ret
  } else {
    error "Ran out of list elements."
  }
}}>]m4_dnl
)

m4_define(_SH_MIN_XTRN_TOOLS,
[<{# ====================================================================
# External tools used by $ourname                                                                   #  List scripts or tools ran or sourced by $ourname.  Define
#  variables below for each external tool, then invoke them using
#  the variable.
# ====================================================================
  # place external tool definitions here..}>]m4_dnl
)

m4_define(_m4_date_pn,m4_patsubst(m4_esyscmd(date '+%Y_%m_%d_%H%M%S'), [<{
}>], [<{}>]))

    #<{

        # '_pn' as in useful in pathname.
        $ _m42 <<< /_m4_date_pn/
        /2017_05_08_085125/

    }>#

m4_define(_m4_date,m4_patsubst(m4_esyscmd(date), [<{
}>], [<{}>]))

    #<{

        $ _m42 <<< /_m4_date/
        /Mon May  8 08:52:17 CDT 2017/

    }>#

m4_define(_m4_bash_RANDOM,[<{m4_esyscmd(echo 'echo -n [<{$}>]RANDOM'|bash)}>])
   m4_dnl Some m4 run syscmd as shell that does not know about $RANDOM, hence the bash above.

m4_define(m4_indent,[<{m4_patsubst($1,[<{^}>],m4_space_chars($2))}>])

    #<{
        $ _m42 <<< "m4_indent([<{abc
        > def
        > ghi
        > }>],8)"
                abc
                def
                ghi

    }>#

m4_define(_SPECIAL_CHARS0,[<{¡><☘''&}>])
    Two double quotes caused problem for csv.

m4_define(m4_space_chars,[<{m4_format([<{%}>]$1[<{s}>],)}>])
m4_define(_ED_spaces, [<{m4_ifelse([<{$1}>], 0, [<{}>], [<{m4_format(%[<{$1}>]s, [<{ }>])}>])}>])
m4_dnl  m4_ifelse so supports '0' spaces w/o any error msg.
m4_dnl TODO swap out all m4_space_chars with _ED_spaces

m4_define(_SH_cmd_subst_assign, [<{m4_dnl
_ED_spaces([<{$1}>])# errexit workaround. errexit in foo() breaks in: x=$(foo 1 2 3)
_ED_spaces([<{$1}>])_SH_cmd_subst_assign_tmpf=$(mktemp ~/tmp/$2[<{.XXXXX}>])
_ED_spaces([<{$1}>])$3 $4 > $_SH_cmd_subst_assign_tmpf
_ED_spaces([<{$1}>])$2=$(< $_SH_cmd_subst_assign_tmpf)
_ED_spaces([<{$1}>])rm -f $_SH_cmd_subst_assign_tmpf
_ED_spaces([<{$1}>])unset _SH_cmd_subst_assign_tmpf}>]m4_dnl
)

    #<{

     # example of errexit issue:
         $ (set -e;foo=$(false;echo hi); echo ho)
         ho
         $ (set -e;foo=$(false); echo ho)
         $ (set -e;foo=$(echo hi;false); echo ho)
         $


    $ _m42 <<< '_SH_cmd_subst_assign([<{4}>],[<{rem_cfg_dir}>], [<{rem_cfg_dir}>], [<{-a asd}>])'
        # errexit workaround. errexit in foo() breaks in: x=$(foo 1 2 3)
        _SH_cmd_subst_assign_tmpf=$(mktemp ~/tmp/rem_cfg_dir.XXXXX)
        rem_cfg_dir -a asd > $_SH_cmd_subst_assign_tmpf
        rem_cfg_dir=$(< $_SH_cmd_subst_assign_tmpf)
        rm -f $_SH_cmd_subst_assign_tmpf
        unset _SH_cmd_subst_assign_tmpf

    }>#

m4_define(_LINES2LINE, [<{m4_dnl
m4_esyscmd([<{_linessquashflat <<\__endofhd
    [<{$1}>]
__endofhd
}>])}>]m4_dnl
)


#<{
Example of _LINES2LINE use in ~/.goldenpod/podcasts.conf._full.markdown._m4

test case:

m4_divert(0)m4_dnl
_LINES2LINE([<{
kaj
akjhkj
ahkj
ahkjhk
ajlk
}>]m4_dnl
)

}>#

m4_define(_MD2HTML_STANDALONE, [<{m4_dnl
m4_esyscmd([<{pandoc  -s -f markdown -t html   <<\_eohd__
[<{$1}>]
_eohd__
}>])}>]m4_dnl
)

#<{
Tried m4_esyscmd_s above, but it causes trouble!

test case
_MD2HTML_STANDALONE( [<{ <https://www.youtube.com/watch?v=ql0Op1VcELw> }>])


}>#


m4_divert(0)m4_dnl
